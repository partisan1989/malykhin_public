package Package1;


public class People_Age {
    public static void main(String[] args) {
        String[][] Persons =
                {
                        {"Anton", "Ivan", "Dima", "Jack", "Kira", "Lena", "Dasha", "Oleg", "Petr", "Vitek"},
                        {"2008", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976"}
                };

        //convert BirthYear from String to int
        int[] BirthYears;
        BirthYears = new int[10];

        for (int i = 0; i < 10; i++) {
            BirthYears[i] = Integer.parseInt(Persons[1][i]);
        }

        int CurrentYear = 2016;

        //Age calculation and output
        int[] PersonAges;
        PersonAges = new int[10];

        String[] StringAges;
        StringAges = new String[10];
        String AgeEnding; //age formatted output

        for (int i = 0; i < 10; i++) {
            PersonAges[i] = CurrentYear - BirthYears[i];
            StringAges[i] = Integer.toString(PersonAges[i]);
            String SecondDigit = StringAges[i].substring(1); //the last age digit

            switch (SecondDigit) {
                case "1":
                    AgeEnding = "Год";
                    break;
                case "2":
                case "3":
                case "4":
                    AgeEnding = "Года";
                    break;
                case "0":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    AgeEnding = "Лет";
                    break;
                default:
                    AgeEnding = "Invalid Age";
                    break;
            }
            System.out.println(Persons[0][i] + " - " + PersonAges[i] + " " + AgeEnding);
        }
    }
}
