package Package1;


public class Main {

    //outputs country population to console
    public static void Population_Output(String Country, long Population) {
        System.out.println(Country + ": " + Population + " people");
    }

    //calculates square in meters
    public static int Calculation_Square_meters(int Country_Square) {
        int Country_Square_meters = Country_Square * 1000;
        return Country_Square_meters;
    }

    //calculates squae in miles
    public static double Calculation_Square_miles(int Country_Square) {
        double Country_Square_miles = Country_Square * 0.621371;
        return Country_Square_miles;
    }

    public static void main(String[] args) {
        //country populations
        long Germany_Population = 345656785;
        long England_Population = 56575767;
        long Poland_Population = 7668756;

        //square in kilometers
        int German_Square_km = 345;
        int England_Square_km = 456;
        int Poland_Square_km = 233;

        //calls output for country populations
        Population_Output("Germany", Germany_Population);
        Population_Output("England", England_Population);
        Population_Output("Poland", Poland_Population);

        //convert square to meters and miles
        int German_Square_m = Calculation_Square_meters(German_Square_km);
        double German_Square_miles = Calculation_Square_miles(German_Square_km);

        int England_Square_m = Calculation_Square_meters(England_Square_km);
        double England_Square_miles = Calculation_Square_miles(England_Square_km);

        int Poland_Square_m = Calculation_Square_meters(Poland_Square_km);
        double Poland_Square_miles = Calculation_Square_miles(Poland_Square_km);

        //output square calculation results
        System.out.println();

        System.out.println("Germany square = " + German_Square_km + " sq. km / " + German_Square_m + " sq. meters / " + German_Square_miles + " sq. miles");
        System.out.println("England square = " + England_Square_km + " sq. km / " + England_Square_m + " sq. meters / " + England_Square_miles + " sq. miles");
        System.out.println("Poland square = " + Poland_Square_km + " sq. km / " + Poland_Square_m + " sq. meters / " + Poland_Square_miles + " sq. miles");

    }
}