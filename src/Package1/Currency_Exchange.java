package Package1;

public class Currency_Exchange {

    public static void main(String[] args) {

        float[] CurrencyRates;
        CurrencyRates = new float[3];

        CurrencyRates[0] = 1.5f;
        CurrencyRates[1] = 3.25f;
        CurrencyRates[2] = 0.23f;

        int InitialSum = 5000;
        float ResultSum = 0;

        System.out.println("Initial UAH sum: " + InitialSum);

        for (int i = 0; i < 3; i++) {
            ResultSum = InitialSum * CurrencyRates[i];
            System.out.println("Converted sum for rate " + i + " is: " + ResultSum);
        }
    }
}