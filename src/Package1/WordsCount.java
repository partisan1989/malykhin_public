package Package1;

/**
 * Created by partisan on 29.11.2016.
 */
public class WordsCount {
    public static void main(String[] args) {
        String mytext = "I have a cat and a dog";
        int max = mytext.length();
        int wordsCount = 0;

        for (int i = 0; i < max; i++) {
            if (mytext.charAt(i) != ' ')
                continue;

            wordsCount++;
        }
        wordsCount++;  //add the first word
        System.out.println(wordsCount);
    }
}
