package Package1;
import java.util.Scanner;

public class Tasks2
{

    public static void main(String[] args)
    {
      //  int a = numberInput();
      //  int b = numberInput();
      //  int c = numberInput();

      //  maxNumber(a,b,c);

//        a = numberInput();
//        b = numberInput();
//        c = numberInput();
//        System.out.println();

     //   Equation(a, b, c);

        operationsWithString();
    }

    public static void maxNumber(int a, int b, int c)
    {
        int firstMaxNumber = 0;
        int secondMaxNumber = 0;
        int resultSquare = 0;

        if ((a > b) && (b > c)) {
            firstMaxNumber = a;
            secondMaxNumber = b;
        } else if ((a > b) && (c > b)) {
            firstMaxNumber = a;
            secondMaxNumber = c;
        } else if ((b > a) && (a > c)) {
            firstMaxNumber = b;
            secondMaxNumber = a;
        } else if ((b > a) && (c > a)) {
            firstMaxNumber = b;
            secondMaxNumber = c;
        } else if ((c > a) && (a > b)) {
            firstMaxNumber = c;
            secondMaxNumber = a;
        } else {
            firstMaxNumber = c;
            secondMaxNumber = b;
        }

        resultSquare = firstMaxNumber * firstMaxNumber + secondMaxNumber * secondMaxNumber;
        System.out.println("Sum of squares: " + resultSquare);
    }

        public static void operationsWithString()
        {
            int numberOfRows = 3;
            String[] arrayOfRows = new String[numberOfRows];
            int[] arrayOfRowsLength = new int[numberOfRows];

            //enter and save rows
            for (int i = 0; i < numberOfRows; i++) {
                System.out.println(String.format("Enter the row number %d: ", i + 1));
                Scanner scan = new Scanner(System.in);
                String row = scan.next();
                arrayOfRows[i] = row;
                arrayOfRowsLength[i] = row.length();
            }

            //init variables for saving row with min length
            int minLength = 0;

            int totalLength = 0;
            double avgLength = 0;

            String minLengthRow = arrayOfRows[0];
            minLength = arrayOfRowsLength[0];

            //cycle for finding row with min length
            for (int i = 0; i < numberOfRows; i++) {

                totalLength = totalLength + arrayOfRowsLength[i];

                if (minLength < arrayOfRowsLength[i]) {
                    continue;
                } else {
                    minLength = arrayOfRowsLength[i];
                    minLengthRow = arrayOfRows[i];
                }
            }

            //print row with min length
            System.out.println("The smallest row is: " + minLengthRow);
            System.out.println("Length: " + minLength);

            avgLength = totalLength / numberOfRows;
            System.out.println("Average row length is " + avgLength);

            //cycle to define row with length < average row length
            for (int i = 0; i < numberOfRows; i++) {
                if (arrayOfRowsLength[i] < avgLength) {
                    System.out.println(arrayOfRows[i]);
                } else {
                    continue;
                }
            }

            int j;
            boolean flag = true;   // устанавливаем наш флаг в true для первого прохода по массиву
            String temp;   // вспомогательная переменная

            while (flag)
            {
                flag = false;    // устанавливаем флаг в false в ожидании возможного свопа (замены местами)
                for (j = 0; j < numberOfRows - 1; j++)
                {
                    if (arrayOfRowsLength[j] > arrayOfRowsLength[j + 1])
                    { // измените на > для сортировки по возрастанию
                        temp = arrayOfRows[j];         // меняем элементы местами
                        arrayOfRows[j] = arrayOfRows[j + 1];
                        arrayOfRows[j + 1] = temp;
                        flag = true;  // true означает, что замена местами была проведена
                    }

                }
            }
            for (int i = 0; i < numberOfRows; i++)
            {
                System.out.println(arrayOfRows[i]);
            }

        }

    public static int numberInput()

    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Input number: ");
        int number = scan.nextInt();
        return number;
    }

    public static void Equation(int a, int b, int c)
    {
        int D = 0;
        double x1 = 0;
        double x2 = 0;

        D = b * b - 4 * a * c;

        if (D == 0)
        {
            x1 = -(b / (2 * a));
            System.out.println(x1);
        }
        else if (D>0)
        {
            x1=(-b - Math.sqrt(b*b-4*a*c))/(2*a);
            x2=(-b + Math.sqrt(b*b-4*a*c))/(2*a);
            System.out.println("x1= " +x1);
            System.out.println("x2= " +x2);
        }
        else System.out.println("Invalid Discriminant");
    }



}
